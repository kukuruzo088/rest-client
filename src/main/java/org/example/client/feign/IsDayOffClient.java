package org.example.client.feign;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "IsDayOffClient", url = "${isdayoff.api.url}")
public interface IsDayOffClient
{
    @GetMapping("/getdata")
    @Cacheable(cacheNames = "isDayOf")
    String getData(@RequestParam(name = "year") Integer year, @RequestParam(name = "month") Integer moth);
}
