package org.example.controller;

import org.example.data.IsDayOffRequest;
import org.example.data.Salary;
import org.example.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MainController {
    private final SalaryService salaryService;

    @Autowired
    public MainController(SalaryService salaryService) {
        this.salaryService = salaryService;
    }

    @GetMapping("/hourly-rate")
    public Salary getHourlyRate(@RequestBody @Valid IsDayOffRequest request) {
            return salaryService.getSalaryInfo(request);
    }
}
