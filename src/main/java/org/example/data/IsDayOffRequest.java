package org.example.data;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

public class IsDayOffRequest
{
    @NotNull
    private Integer year;

    @Range(min = 1, max = 12)
    private Integer month;

    @NotNull
    private Double salary;

    public IsDayOffRequest() {}

    public IsDayOffRequest(Integer year, Integer month, Double salary) {
        this.year = year;
        this.month = month;
        this.salary = salary;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }
}
