package org.example.data;


import java.time.Month;

public class Salary
{
    private Integer year;
    private Month moth;
    private Double salary;
    private Double hourIncome;

    public Salary() {}

    public Salary(Integer year, Month moth, Double salary, Double hourIncome) {
        this.year = year;
        this.moth = moth;
        this.salary = salary;
        this.hourIncome = hourIncome;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Month getMoth() {
        return moth;
    }

    public void setMoth(Month moth) {
        this.moth = moth;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getHourIncome() {
        return hourIncome;
    }

    public void setHourIncome(Double hourIncome) {
        this.hourIncome = hourIncome;
    }
}
