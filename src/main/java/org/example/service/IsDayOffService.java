package org.example.service;

import org.example.client.feign.IsDayOffClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IsDayOffService
{
    private final IsDayOffClient client;

    @Autowired
    public IsDayOffService(IsDayOffClient client) {
        this.client = client;
    }

    public String getData(Integer year, Integer month) {
        return client.getData(year, month);
    }

    public Integer getWorkDaysInMonth(Integer year, Integer month) {
        String daysOnMonth = getData(year, month);
        return getWorkDay(daysOnMonth);
    }

    private Integer getWorkDay(String days) {
        int daysCount = 0;
        for(Character day: days.toCharArray()) {
            if(day.equals('0')) {
                daysCount += 1;
            }
        }
        return daysCount;
    }
}
