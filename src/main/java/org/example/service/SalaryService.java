package org.example.service;

import org.example.data.IsDayOffRequest;
import org.example.data.Salary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Month;

@Service
public class SalaryService {
    private final IsDayOffService isDayOffService;

    @Autowired
    public SalaryService(IsDayOffService isDayOffService) {
        this.isDayOffService = isDayOffService;
    }

    public Salary getSalaryInfo(IsDayOffRequest request) {
        Integer workDaysInMonth = isDayOffService.getWorkDaysInMonth(request.getYear(), request.getMonth());
        BigDecimal hourIncome = BigDecimal.valueOf(getHourIncome(request.getSalary(), workDaysInMonth));
        return new Salary(
                request.getYear(),
                Month.of(request.getMonth()),
                request.getSalary(),
                hourIncome.setScale(2, RoundingMode.HALF_UP).doubleValue()
        );
    }

    private Double getHourIncome(Double salary, Integer workDaysInMonth) {
        return salary / (workDaysInMonth * 8);
    }
}
