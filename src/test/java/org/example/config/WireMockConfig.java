package org.example.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class WireMockConfig
{
    @Bean(initMethod = "start", destroyMethod = "stop")
    public WireMockServer mockOpenExchangeRatesClient() {
        return new WireMockServer(9561);
    }
}
