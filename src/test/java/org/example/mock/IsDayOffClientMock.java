package org.example.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.IOException;

import static java.nio.charset.Charset.defaultCharset;
import static org.springframework.util.StreamUtils.copyToString;

public class IsDayOffClientMock
{
    public static void setupIsDayOffClientMock(WireMockServer mockService) throws IOException {
        mockService.stubFor(WireMock.get(WireMock.urlPathMatching("/getdata"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.TEXT_PLAIN_VALUE)
                        .withBody(copyToString(
                                IsDayOffClientMock.class.getClassLoader()
                                        .getResourceAsStream("payload/get-is-day-off-response.txt"),
                                defaultCharset()))));
    }
}
