package org.example.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.example.config.WireMockConfig;
import org.example.data.IsDayOffRequest;
import org.example.data.Salary;
import org.example.mock.IsDayOffClientMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@EnableConfigurationProperties
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {WireMockConfig.class})
class SalaryServiceTest {
    @Autowired
    private WireMockServer wireMockServer;

    @Autowired
    private SalaryService salaryService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private IsDayOffRequest request;

    @BeforeEach
    void setUp() throws IOException {
        IsDayOffClientMock.setupIsDayOffClientMock(wireMockServer);
        Random random = new Random();
        Calendar calendar = Calendar.getInstance();
        request = new IsDayOffRequest(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1,
                random.nextDouble() * 1000
        );
    }

    @Test
    public void getSalaryInfoTest() {
        Salary salaryInfo = salaryService.getSalaryInfo(request);
        assertFalse(salaryInfo.getHourIncome().isNaN());
    }

    @Test
    public void getHourlyRateTest() throws Exception {
        mockMvc.perform(get("/hourly-rate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hourIncome").isNumber());
    }
}